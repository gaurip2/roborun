# RoboRun

## Getting started

Run `git lfs clone https://gitlab.engr.illinois.edu/gaurip2/roborun.git` to clone the project folder.

## Debugging tips
 - [Snipper isn't following player?]
    - Is there a NavMesh in the level ? Does the NavMesh cover the entire level (click P to check the area NavMash covers)
 

## Level Packages

We use the following 3D-party provided packages in our game:

- https://www.unrealengine.com/marketplace/en-US/product/spaceship-interior-environment-set
- https://www.unrealengine.com/marketplace/en-US/product/modular-scifi-season-2-starter-bundle
- https://www.unrealengine.com/marketplace/en-US/product/modular-scifi-season-1-starter-bundle
- https://www.unrealengine.com/marketplace/en-US/product/fps-weapon-bundle

## Authors and acknowledgment
Ishani Tarafdar
Rachel Huang
James Huang
Gauri Patwardhan
